package deaminimizer;

/**
 * AlgorithmException
 */
public class AlgorithmException extends Exception {

    /**
     * Constructor.
     */
    public AlgorithmException() {
    }

    /**
     * Constructor with error message.
     * @param msg
     */
    public AlgorithmException(String msg) {
        super(msg);
    }
}
