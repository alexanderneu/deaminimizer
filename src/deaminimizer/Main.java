package deaminimizer;
import com.martiansoftware.jsap.*;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            SimpleJSAP jsap = new SimpleJSAP("DEAminimizer", "Minimizes a deterministic finite automaton!",
                new Parameter[]{
                    new FlaggedOption("filename", JSAP.STRING_PARSER, null, JSAP.NOT_REQUIRED, 'f', JSAP.NO_LONGFLAG,
                                        "A file with a deterministic finite automaton"),
                    new FlaggedOption("dontMinimize", JSAP.BOOLEAN_PARSER, "false", JSAP.NOT_REQUIRED, 'm', JSAP.NO_LONGFLAG, "Prevent minimization"),
                    new FlaggedOption("dfa", JSAP.STRING_PARSER, null, JSAP.NOT_REQUIRED, 'd', JSAP.NO_LONGFLAG,
                                        "A deterministic finite automaton.\nSyntax: {statename; statename2; 'symbol'}\n+statename for start state (must be in the first line!)\n!statename for accept state")
                });

            JSAPResult config = jsap.parse(args);
            if (jsap.messagePrinted()) // --help
                System.exit(1);

            String filename = config.getString("filename"); // -f dfafile.txt

            Automaton automat = new Automaton();
            try {
                if(filename != null)
                    automat.parseFile(filename);
                else
                    if(!config.getString("dfa").equals(""))
                        automat.parseString(config.getString("dfa")); // -d "{+s0; s1; '0'}{s0; s0; '1'}{s1; !s2; '1'}"
                    else
                    {
                        System.out.println("Missing dfa (\"--help\" for help)");
                        System.exit(0);
                    }
            } catch (AlgorithmException ex) {
                System.out.println(ex.getMessage());
            } catch(IOException ex)
            {
                System.out.println(ex.getMessage());
            }

            boolean dontMinimize = config.getBoolean("dontMinimize"); // -m true
            if(!dontMinimize)
                automat.minimize();
            //else
            //    System.out.println("minimization prevented");
            
            System.out.println(automat.toString());

        } catch (JSAPException ex) {
        }
    }
}
