package deaminimizer;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Die State-Klasse.
 */
public class State {

    static ArrayList<State> states_scanned = new ArrayList<State>();
    static ArrayList<String> symbolArray = new ArrayList<String>();
    static ArrayList<State> mini_states = new ArrayList<State>();
    private String name = "";
    private boolean isAcceptState = false;
    private ArrayList<Transition> transitions = new ArrayList<Transition>();   

    
    public State(){
        
    }
    /**
     * Constructor.
     * @param name
     */
    public State(String name) {
        this.name = name;
    }

    /**
     * Constructor.
     * @param name
     * @param isAcceptState
     */
    public State(String name, boolean isAcceptState) {
        this.name = name;
        this.isAcceptState = isAcceptState;
    }

    /**
     * Adds an transition to the State.
     * @param tran Transition
     */
    public void addTransition(Transition tran) {
        tran.setFrom(this);
        transitions.add(tran);
    }

    /**
     * Returns all Transitions.
     * @return
     */
    public ArrayList<Transition> getTransitions() {
        return transitions;
    }

    /**
     * Returns the name of the state.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns if the state is accept state.
     * !!! WARNING !!!: Before calling this method please call State.states_scanned.clear(); !!!
     * @return
     */
    public State getState(String name) {
        if(!states_scanned.contains(this))
            states_scanned.add(this);
        
        if (this.name.equals(name)) {
            return this;
        } else {
            for (Transition trans : transitions) {
                if (!states_scanned.contains(trans.getTo())) {
                    trans.getTo().getState(name);
                }
            }
            for (State s : states_scanned) {
                if (s.getName().equals(name)) {
                    return s;
                }
            }
        }
        return null;
    }

    /**
     * Returns the accept state if it exists.
     * !!! WARNING !!!: Before calling this method please call State.states_scanned.clear(); !!!
     * @return
     */
    public State getAcceptState() {
        if(!states_scanned.contains(this))
            states_scanned.add(this);
        
        if (this.isAcceptState) {
            return this;
        } else {
            for (Transition trans : transitions) {
                if (!states_scanned.contains(trans.getTo())) {
                    trans.getTo().getAcceptState();
                }
            }
            for (State s : states_scanned) {
                if (s.isAcceptState) {
                    return s;
                }
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return "[Name: " + name + "]";//+ ((isAcceptState) ? ", isAcceptState: " + isAcceptState : "") 
    }
    
    public boolean acceptedState(){
        return this.isAcceptState;
    }
    
    public void defineSymbols(){
        for(Transition tr: transitions){
            if(!symbolArray.contains(tr.getSymbol())){
                symbolArray.add(tr.getSymbol());
            }            
        }
    }
    
    public State getToState(String symbol){
        for(Transition tr: transitions){
            if(tr.getSymbol().equals(symbol))
                return tr.getTo();
        }
        return this;
    }
    
    public void removeTransitionDuplicates() {
        HashSet<Transition> hashSet = new HashSet<Transition>(transitions);
        this.transitions.clear();
        this.transitions.addAll(hashSet);
   } 

}
