package deaminimizer;

/**
 * Die Transition-Klasse.
 */
public class Transition {

    private State from;
    private State to;
    private String symbol;

    /**
     * Constructor.
     * @param to
     * @param symbol
     */
    public Transition(State to, String symbol) {
        this.to = to;
        this.symbol = symbol;
    }

    /**
     * Used by {@link State#addTransition(deaminimizer.Transition)}.
     * @param fromState
     */
    public void setFrom(State fromState) {
        from = fromState;
    }

    /**
     * Used by {@link State#getState(java.lang.String)} and {@link State#getAcceptState()}.
     */
    public State getTo() {
        return to;
    }
    
    public String getSymbol(){
        return symbol;
    }
    
    public State getFrom(){
        return from;
    }
    
    @Override
    public String toString() {
        return "\n{From: " + from + ", To: " + to + ", Symbol: " + symbol + "}";
    }
    
    public boolean compareTo(State s, Transition t){
        t.setFrom(s);
        if(this.from.getName().equals(t.from.getName()) && this.to.getName().equals(t.to.getName()) && this.symbol.equals(t.symbol))
            return true;
        return false;
    }
}
