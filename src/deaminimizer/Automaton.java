package deaminimizer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.MatchResult;

/**
 * Die Automaton-Klasse.
 */
public class Automaton {

    private State start; // the start state
    private String dfa_data = ""; // the imported dfa, which we should parse
    private int[][] Matrix;

    /**
     * Parsing a DFA.
     * The data is in a String.
     * @param dfa The DFA-String to parse.
     */
    public void parseString(String dfa) throws AlgorithmException {
        dfa_data = dfa;
        parse();
    }

    /**
     * Parsing a DFA.
     * The data is in a File.
     * @param filePath The filepath to the DFA.
     * @throws IOException
     */
    public void parseFile(String filePath) throws IOException, AlgorithmException {
        dfa_data = readFileAsString(filePath);
        parse();
    }

    /**
     * Parsing a DFA into our own classes.
     */
    private void parse() throws AlgorithmException {
        start = null;
        Scanner s = new Scanner(dfa_data);
        do {
            try {
                s.findInLine("([+|!]??\\w+);([ ]*)([+|!]??\\w+);([ ]*)(['|\"]??)(\\w+)(['|\"]??)");
                MatchResult result = s.match();
                if (result.groupCount() > 0) {
                    if (result.group(1).startsWith("+")) { // Wenn der Name mit einem + beginnt, ist er ein Startzustand
                        if (start != null && start.getName().equals(result.group(1).substring(1))) { // Gibt es start und ist der Name der selbe?
                            start.addTransition(new Transition(getOrCreateState(result.group(3)), result.group(6))); // finde oder erstelle einen State und füge einen Übergang zu start hinzu
                        } else if (start == null) { // wenn start existiert, der Name nicht der selbe ist, dann Fehlermeldung auswerfen:
                            start = new State(result.group(1).substring(1));
                            start.addTransition(new Transition(getOrCreateState(result.group(3)), result.group(6)));
                        } else {
                            throw new AlgorithmException("Parse error: Start state already set!");
                        }
                    } else if (result.group(1).startsWith("!")) { // Wenn der Name mit ! beginnt:
                        if (start == null) {
                            throw new AlgorithmException("Parse error: Start state is not set! Must be the first State!");
                        } else {
                            State temp = start.getState(result.group(1).substring(1));
                            if (temp != null) { // Endzustand existiert
                                State.states_scanned.clear();
                                temp.addTransition(new Transition(getOrCreateState(result.group(3), result.group(3).startsWith("!")), result.group(6))); // neue Transition erstellen mit State finden/erstellen (mit getOrCreateState) dabei übergeben, ob es ein Endzustand sein soll
                            }
                        }
                    } else { // Wenn der Name nicht mit + oder ! beginnt:
                        if (start == null) { // Gibt es start noch nicht? Dann Fehler!
                            throw new AlgorithmException("Parse error: Start state is not set! Must be the first State! First state not found!");
                        } else {
                            // State finden (start.getState)
                            State.states_scanned.clear();
                            State temp = start.getState(result.group(1));
                            
                            String newName = result.group(3);
                            if (result.group(3).startsWith("!"))
                                newName = newName.substring(1);
                            
                            State temp2 = getOrCreateState(newName, result.group(3).startsWith("!"));
                            if(temp != null)
                                temp.addTransition(new Transition(temp2, result.group(6)));
                        }
                    }

                }
            } catch (IllegalStateException ex) {
                break;
            }
        } while (true);
        s.close();

        State.states_scanned.clear();
        if (start != null && start.getAcceptState() == null) {
            start = null;
            throw new AlgorithmException("Parse error: Accept state is not set!");
        }

        dfa_data = "";
    }

    /**
     * Minimizes the DFA.
     */
    public void minimize() {
        //Define all Symbols in teh DEA
        start.defineSymbols();
        
        // ausgabe von einer Matrix
        this.Matrix = new int [State.states_scanned.size()][State.states_scanned.size()];
        initMatrixFill();   // initialisiert die Matrix
        prozessMatrix();    // verarbeitet die Matrix weiter...
        packMatrix();       // setzt States zusammen und ordnet die Transitions hinzu
        System.out.println("Der DEA als Matrix-Ausgabe:");
        outputMatrix();     // gibt die Matrix aus...
        writeToFile();      // schreibt den neuen DEA in die miniDEA.txt
    }

    /**
     * Returns the current DFA as String.
     * @return String
     */
    @Override
    public String toString() {
        return "";//"toString() not yet implemented";
    }

    /**
     * Returns a State whether it exists or it creates a new one.
     * @param name of the State
     * @return the State or new State
     */
    private State getOrCreateState(String name) throws AlgorithmException {
        return getOrCreateState(name, false);
    }

    /**
     * Returns a State whether it exists or it creates a new one.
     * @param name of the State
     * @param isAcceptState 
     * @return the State or new State
     */
    private State getOrCreateState(String name, boolean isAcceptState) throws AlgorithmException {
        State.states_scanned.clear();
        State temp = getState(name); // call getState
        if (temp != null) { // if a State got returned (so it exists)
            return temp; // return it also here
        } else {
            return new State(name, isAcceptState); // else create new state
        }
    }

    /**
     * Returns a State whether it exists or not.
     * @param name of the State
     * @return the State or null
     */
    private State getState(String name) {
        if (start != null) {
            return start.getState(name);
        }
        return null;
    }

    /**
     * Reads a file into a String.
     * @param filePath
     * @return
     * @throws java.io.IOException
     */
    private static String readFileAsString(String filePath) throws java.io.IOException {
        byte[] buffer = new byte[(int) new File(filePath).length()];
        BufferedInputStream f = null;
        try {
            f = new BufferedInputStream(new FileInputStream(filePath));
            f.read(buffer);
        } finally {
            if (f != null) {
                try {
                    f.close();
                } catch (IOException ignored) {
                }
            }
        }
        return new String(buffer);
    }
    /*
     *  Schreibt unseren Automaten in die Datei "minimizedAutomaton.txt"
     */
    private void writeToFile(){
        FileWriter writer;
        File file = new File("miniDEA.txt");
        String str = "";  // String in dem alles gepackt wird.
        
        try{
            writer = new FileWriter(file ,false); // false => rewrite (lösche die alte Datei)
            for(State s : State.mini_states){
                for(int i = 0; i < s.getTransitions().size(); i++){ // durchgehe jede Transition des States um diese auszugeben
                    // ...{+HerkunftsState;...
                    if(s.getName().indexOf(start.getName()) != -1){ // Wenn der State der start-State ist/war schreibe + davor
                        str = str+"{+"+s.getName()+";";
                    }// ...{!HerkunftsState;...
                    else if(s.acceptedState()){ // Wenn acceptedState() == true ist es der Endzustand den wir mit ! Kennzeichnen
                        str = str+"{!"+s.getName()+";";
                    }
                    else{
                        // ...{HerkunftsState;...
                        str = str+"{"+s.getName()+";";
                    }
                    
                    // ...+ZielState;...
                    if(s.getTransitions().get(i).getTo().getName().indexOf(start.getName()) != -1){ // Wenn der State der start-State ist/war schreibe + davor
                        str = str+"+"+s.getTransitions().get(i).getTo().getName()+";";
                    }// ...!ZielState;...
                    else if(s.getTransitions().get(i).getTo().acceptedState()){ // Wenn acceptedState() == true ist es der Endzustand den wir mit ! Kennzeichnen
                        str = str+"!"+s.getTransitions().get(i).getTo().getName()+";";
                    }
                    else{
                        // ...ZielState;...
                        str = str+s.getTransitions().get(i).getTo().getName()+";";
                    }
                    
                    // ...Symbol;}...
                    str = str+"'"+s.getTransitions().get(i).getSymbol()+"'}"+System.getProperty("line.separator");
                }
            }
            // Text wird in den Stream geschrieben
            writer.write(str);
            System.out.println("\nMinimized:\n"+str);
            // Schreibt den Stream in die Datei
            writer.flush();
            
            // Bestätigung ausgeben
            System.out.println("Der DEA wurde erfolgreich nach miniDEA.txt minimiert.");
        } catch (IOException e){
            System.out.println("Schreiben fehlgeschlagen:\n"+ e);
        }
        
    }
    
    /**
     * Einfach nur um die gefüllte Matrix zu sehen
     */
    public void outputMatrix(){
        //System.out.print("\n"+state+" | ");
        System.out.print("   | z0 | z1 | z2 | z3 | z4 |\n");
        for(int y=Matrix.length-1;0 <= y;y--){
            State str = State.states_scanned.get(y);
            System.out.print(str.getName()+" | ");
            for(int x=0;x < Matrix[y].length;x++){
                    System.out.print(Matrix[x][y]+"  | ");
            }
            System.out.println();
        }
    }
    
    /**
     * Matrix füllen: Die Werte die von Anfang an gleich sind, werden mit einer 2 Markiert - Andere Folgezust&auml;nde mit 1
     */
    private void initMatrixFill(){
        for(int y=0;y < State.states_scanned.size();y++){   //Eingescannte States durchlaufen
            State p = State.states_scanned.get(y);   //Nur gesetzt damit es schöner aussieht
            for(int x=0;x < State.states_scanned.size();x++){ //Eingescannte States die zweite
                State q = State.states_scanned.get(x); //Wieder nur damit es schön aussieht
                if(p.getName().equals(q.getName())){    //Wenn die zustände von anfang an gleich sind z.B. z0 & z0
                    Matrix[x][y] = 2;                   //werden mit 2 Markiert damit diese nicht berücksichtigt werden
                }
                if(p.acceptedState() == true & q.acceptedState() == false){ //wenn der eine nen Endzustand ist und der andere nicht wird die Stelle markiert
                    Matrix[x][y] = 1;                                       //mit 1
                }
            }
        }
    }
    
    /**
     * Durchläuft die Matrix und guckt was Mr.Smith markiert hat...
     */
    private void prozessMatrix(){
        //Variablen die gebraucht werden zum zwischenspeichern von irgendwelchen Werten
        State zw1 = new State();
        State zw2 = new State();
        int _y1 = 0;
        int _x1 = 0;
        int _y2 = 0;
        int _x2 = 0;
        //-------------Debug zwecke
        String output = "";
        //-------------Debug zwecke
        for(int y=(State.states_scanned.size()-1);0<=y;y--){ // Von hinten ist schöner :P 
            State p = State.states_scanned.get(y);   //Schönheit
            for(int x=(State.states_scanned.size()-1);0<=x ;x--){ //Und nochmal von hinten
                State q = State.states_scanned.get(x);  //Schönheit     
                for(String s : State.symbolArray){ //Durchlaufe alle Symbole und guck was passiert wenn zw1 und zw2 das Symbol bekommen
                    zw1 = p.getToState(s);
                    zw2 = q.getToState(s);
                    _x2 = State.states_scanned.indexOf(zw1);
                    _y2 = State.states_scanned.indexOf(zw2);
                    _x1 = State.states_scanned.indexOf(p);
                    _y1 = State.states_scanned.indexOf(q);

                    /*//----- Debug zwecke
                    output += "\n---------------------------------------------------------------------------------------------";
                    output += "\n1. "+p.getName()+"(index: "+_x1+") mit "+s+" nach "+zw1.getName()+"(index: "+_x2+")";
                    output += "\n2. "+q.getName()+"(index: "+_y1+") mit "+s+" nach "+zw2.getName()+"(index: "+_y2+")";
                    output += "\nErgibt stelle: ["+_x2+" - "+_y2+"] Dort steht: "+Matrix[_y2][_x2];
                    output += "\nAlte stelle: ["+_x1+" - "+_y1+"] Dort steht: "+Matrix[_x1][_y1]+" ";
                    //------Debug zwecke*/
                    if(Matrix[_x2][_y2] == 1){  //Wenn an der stelle wo vorheriges hinführt eine 1 steht werden die Zustände die darauf gezeigt haben auch markiert
                        Matrix[x][y] = 1;       //Einmal x&y und _x1&_y1 Grund: Damit das auf beiden Seiten der Matrix markiert wird.
                        Matrix[_x1][_y1] = 1;
                        /*//----- Debug zwecke
                        output += "\nDann sollte stelle: ["+_x1+" - "+_y1+"] Dort steht: "+Matrix[_x1][_y1]+" wurde gesetzt";
                        //----- Debug zwecke*/
                    }
                    /*//----- Debug zwecke
                    output += "\n---------------------------------------------------------------------------------------------\n";
                    //----- Debug zwecke*/
                }
            }            
        }
    }
    
    /**
     * Verpackt die in der vorherigen Methode ergebenen Werte
     */
    private void packMatrix(){
        ArrayList<Transition> trans = new ArrayList<Transition>(); //Temporäre ArrayListe mit allen Transitions
        boolean _contains = false;
        
        // Erstelle PrüfArray ob Zustand zusammengelegt wurde oder nicht
        boolean temp_assembled[] = new boolean[Matrix.length];
        for(int i = 0; i < Matrix.length; i++){
            temp_assembled[i] = false;
        }
        
        for(int y = 0;y<Matrix.length;y++){
            for(int x = 0;x<Matrix[y].length;x++){
                if(Matrix[x][y] == 0){  //Wenn die stelle in der Matrix ne 0 ist dann können die Zustände zusammen gepackt werden
                    for(State st: State.mini_states){   //States zusammen packen
                        String s1 = State.states_scanned.get(x).getName()+","+State.states_scanned.get(y).getName();
                        String s2 = State.states_scanned.get(y).getName()+","+State.states_scanned.get(x).getName();
                        if(st.getName().equals(s1)){ // Wenn schon ein State mit z.B. u,i vorhanden ist wird _contains gesetzt um diesen State nicht nochmal rein zu packen
                            _contains = true; //Take the Blue pill
                        }
                        if(st.getName().equals(s2)){ // gleiches wie oben nur anders herum z.B. i,u
                            _contains = true; //Take the Red pill 
                        }
                    }
                    
                    if(!_contains){ //Wenn die nicht schon vorhanden sind packt der sie rein inkl. allen transitions
                        String _name = State.states_scanned.get(y).getName()+","+State.states_scanned.get(x).getName();
                        boolean _acceptedState = false;
                        if(State.states_scanned.get(x).acceptedState() & State.states_scanned.get(y).acceptedState())
                            _acceptedState = true; 
                        trans.addAll(State.states_scanned.get(y).getTransitions());
                        trans.addAll(State.states_scanned.get(x).getTransitions());
                        State.mini_states.add(new State(_name,_acceptedState));
                    }
                    // wenn Matrix[x][y] == 0 (Zustände zusammengesetzt werden), setze im Prüfarray auf true
                    temp_assembled[x] = true;
                }
            }           
        }
        
        // Guckt nach welche States nicht zusammen gesetzt wurden und packt sie in mini_states
        for(int i = 0; i<temp_assembled.length; i++){
            if(!temp_assembled[i]){
                trans.addAll(State.states_scanned.get(i).getTransitions());
                State.mini_states.add(new State(State.states_scanned.get(i).getName(), State.states_scanned.get(i).acceptedState()));
            }
        }
        
        
        System.out.println("-------------------------------------------");     
        /*
         * Schleife zur findung der Transitions von Zusammengesetzten zu Zusammengesetzten States
         * - durchgehe die mini_states
         * - suche beide Transitions der Urstates
         * - suche die Ziele der Transitions
         * - prüfe ob die Symbole der Transitions gleich sind
         * -    Ja: schreibe in mini_states die Transition mit Ziel, Symbol
         * -    Nein: Gib Meldung aus und mache weiter
         */
        for(State s : State.mini_states){
            String s1, s2, s1_to="", s2_to="", s1_symb = "", s2_symb = "";
            int s_place = s.getName().indexOf(",");
            
            if(s_place != -1){
                // Splitte den Namen unseres States 's' in s1 und s2 auf...
                s1 = s.getName().substring(0, s_place);
                s2 = s.getName().substring(s_place+1);
                //System.out.println("Pruefe State: ["+s1+","+s2+"]");
                // Hole dir alle Daten über die Transitions von s1 und s2
                // (welche zusammengesetzt wurden)
                for(Transition t : trans){
                    if(t.getFrom().getName().equals(s1)){
                        s1_to = t.getTo().getName();
                        s1_symb = t.getSymbol();
                       //System.out.println("Transition zu s1: "+s1+" mit Ziel: "+s1_to+", Symbol: "+s1_symb+" gefunden.");
                        // da wir eine mögliche Transition gefunden haben suchen wir nun eine für s2...
                        for(Transition _t : trans){
                            if(_t.getFrom().getName().equals(s2)){
                                s2_to = _t.getTo().getName();
                                s2_symb = _t.getSymbol();
                                //System.out.println("Transition zu s2: "+s2+" mit Ziel: "+s2_to+", Symbol: "+s2_symb+" gefunden.");
                                // wir haben nun für s1 und s2 Transitions, nun prüfen wir ob diese auch zusammen gehören
                                // Suche die Zielstates _s (welche zusammengesetzt wurden)
                                for(State _s : State.mini_states){
                                    if(_s.getName().indexOf(s1_to) != -1){ // ist der Name von s1 in dem State enthalten?
                                        if(_s.getName().indexOf(s2_to) != -1){ // ist der Name von s2 in dem State enthalten?
                                            //System.out.println("ZielState "+ _s.getName() +" von ["+s1+","+s2+"] gefunden");
                                            // habe den neuen State (als Ziel) gefunden
                                            // und prüfe ob die Symbole identisch sind
                                            // (was sie theoretisch sein MÜSSEN sonst hat unser Algorythmus-Programmierer versagt)
                                            if(s1_symb.equals(s2_symb)){
                                                Transition tr = new Transition(_s, s1_symb);// erzeuge Transition die von s auf _s mit dem Symbol von _t.getTo() zeigt
                                                if(!checkTransition(s, tr)){ // wenn die Transition noch nicht in _s vorhanden ist, füge diese ein
                                                    s.addTransition(tr);
                                                }
                                            } else{
                                                System.out.println("Fehler mit Symbol:\n"+s1_symb+" & "+s2_symb);
                                                System.out.println(s1+" & "+s2+" wurden zu "+ s.getName() +" zusammgesetzt, besitzen gleiche Ziel-Transitions allerdings mit anderen Symbolen.");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        /* ...durchsucht alle Transitions, um die fehlenden Transitions von zusammengesetzten Zuständen
         * die auf nicht zusammengesetzte Zustände zeigen, zu finden und zu erzeugen.
         * 
         */
        for(Transition t : trans){ // durchgehe alle Transitions 't' ...
            for(State s : State.mini_states){ // durchgehe alle mini_states 's' ...
                if(s.getName().indexOf(t.getTo().getName()) != -1){ // ... wenn der Name von mini_states 's' gleich dem Ziel von Transition 't' ist ...
                    for(Transition _t : trans){ // ... durchgehe alle Transitions '_t' ...
                        if(t.getTo() == _t.getTo() && t.getFrom() != _t.getFrom()){ // ... wenn die Transitions das gleiche Ziel aber eine unterschiedliche Herkunft haben...
                            // ... prüfe ob die zusammengesetzten Namen der Transitions in der Liste von '_s' aus mini_states vorhanden ist ...
                            State _st = new State(_t.getFrom().getName()+","+t.getFrom().getName()); //... erzeuge Objekt von State mit zusammengesetzten Namen der Transitions die auf 's' zeigen
                            int s_temp = 0; // Zähler für State.mini_states.get();
                            for(State _s : State.mini_states){
                                //System.out.println("1: "+_s.getName()+"   2:"+_st.getName()+"     3:"+s.getName());// Debug
                                if(_s.getName().equals(_st.getName())){ // ... suche den Namen von '_st' in '_s'
                                    Transition tr = new Transition(s, t.getSymbol());// erzeuge Transition die von _st auf _s mit dem Symbol von _t.getTo() zeigt
                                    if(!checkTransition(State.mini_states.get(s_temp), tr)){ // wenn die Transition noch nicht in _s vorhanden ist, füge diese ein
                                        State.mini_states.get(s_temp).addTransition(tr);
                                    }
                                    if(!t.getSymbol().contentEquals(_t.getSymbol())){ // ... prüfe ob die Symbole identisch sind, damit beide Symbole als Transition hinzugefügt werden
                                        Transition _tr = new Transition(s, _t.getSymbol());
                                        if(!checkTransition(State.mini_states.get(s_temp), _tr)){ // wenn die Transition noch nicht in _s vorhanden ist, füge diese ein
                                            State.mini_states.get(s_temp).addTransition(_tr);
                                        }
                                    }
                                }
                                s_temp++;
                            }
                        }
                    }
                }
                /**
                 * Durchsucht die mini_states nach nicht zusammen gesetzten States und ordnet derren Transitions zu
                 */
                if(s.getName().indexOf(t.getFrom().getName()) != -1){ // ... wenn der Name von mini_states 's' gleich der Herkunft von Transition 't' ist ...
                    int s_temp = 0; // Zähler für State.mini_states.get();
                    for(State _s : State.mini_states){ //... suche den Namen des Ziels von 't' in mini_states '_s' 
                        if(t.getTo().getName().indexOf(_s.getName()) != -1){
                            Transition tr = new Transition(_s, t.getSymbol());// erzeuge Transition die von s auf _s mit dem Symbol von _t.getTo() zeigt
                            if(!checkTransition(State.mini_states.get(s_temp), tr)){ // wenn die Transition noch nicht in _s vorhanden ist, füge diese ein
                                State.mini_states.get(s_temp).addTransition(tr);
                            }
                        }
                        s_temp++;
                    }
                }
            }
        }
         // Ausgaben zu Test-Zwecken
        for(State s : State.mini_states){
            System.out.println(s.getName());
            System.out.println(s.getTransitions());
        }
        /*
        for(State all : State.states_scanned) {
            System.out.println(all.getTransitions());
        }*/

    }
    
    /**
     * checkTransition(State, Transition)
     * prüft ob die Transition in dem State schon vorhanden ist
     */
    private boolean checkTransition(State s, Transition t){
        for(int i = 0; i < s.getTransitions().size(); i++){
            if(s.getTransitions().get(i).compareTo(s, t))
                return true;
        }
        return false;
    }
    
    /**
     * Haut alle Dublikate aus den Transitions... oder so &auml;hnlich
     * dumme funktion - KANN WEG!
     */
        private void FilterDublicates(){
        for(State g : State.mini_states){
            for(String s : State.symbolArray){
                String index = "";
                for(int i=0;i < g.getTransitions().size();i++){
                    if(g.getTransitions().get(i).getSymbol().equals(s)){
                        index += ","+i;
                    }
                }
                String[] splitstr = index.split(",");
                int[] zu = new int[splitstr.length];
                for(int r = splitstr.length-1;1 < r ;r--){
                    zu[r] = Integer.valueOf( splitstr[r] ).intValue();
                    g.getTransitions().remove(zu[r]);
                }
            }
        }
    }
    
    //TODO: Irgendiwe die restlichen nicht zusammengepackten zustände in mini_states reinpacken...
    /*private void finalizeMatrix(){
        //String[] _name = new String[];
        ArrayList<State> _temp = new ArrayList<State>(State.states_scanned);
        splitNames();
        for(String n : _name){
            for(State h : _temp){
                if(n.indexOf(h.getName()) != -1){
                    State.mini_states.add(h);
                }
            }            
        }
        //System.out.println(_temp);
    }
    
    /*private String[] splitNames(){
        String[] _name;
        for(State s: State.mini_states){
            _name = s.getName().split(",");
        }
        return _name;
    }*/
}
